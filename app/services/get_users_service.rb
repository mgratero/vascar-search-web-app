# frozen_string_liter
class GetUsersService
  include HTTParty
  base_uri Rails.configuration.api_endpoint

  def initialize(fullname)
    @fullname = fullname
  end

  def search
    result = self.class.get(
      '/api/users/search',
      body: { user: { fullname: @fullname } },
      verify: false
    )
    JSON.parse(result.response.body)
  end
end
