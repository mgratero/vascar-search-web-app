# frozen_string_literal: true

class PagesController < ApplicationController
  def landing; end

  def search_user
    @users = GetUsersService.new(params[:fullname]).search
  rescue StandardError
    @users = []
  end
end
